package my.kotlin.application.behere_new_ui.main.woojin

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_group_list.view.*
import my.kotlin.application.behere_new_ui.R

class GroupListAdapter(private val context: Context, private val groupDataList: List<String>) :
    RecyclerView.Adapter<GroupListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.adapter_group_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.groupName.text = groupDataList[position]
        holder.groupName.setOnClickListener {
            if (!it.isSelected) {
               holder.groupName.setTextColor(context.resources.getColor(R.color.grey1))
                it.isSelected = true
            } else {
                holder.groupName.setTextColor(context.resources.getColor(R.color.grey4))
                holder.groupName.setBackgroundResource(R.drawable.edit_background)
                it.isSelected = false
            }
        }

        when(position){ // Group add button
        }

    }

    override fun getItemCount(): Int {
        return groupDataList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val groupName:TextView = itemView.group_name_txt
    }
}