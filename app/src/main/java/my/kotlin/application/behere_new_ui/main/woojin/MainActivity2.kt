package my.kotlin.application.behere_new_ui.main.woojin

import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main2.*
import my.kotlin.application.behere_new_ui.R

class MainActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_main2)

        initializeViews()
    }

    private fun initializeViews() {
        val todoDataList = listOf("오늘 할 일", "내일 할 일", "지금 할 일")

        val todoList: RecyclerView = todo_list
        todoList.layoutManager = LinearLayoutManager(this)
        todoList.adapter = TodoListAdapter2(this, todoDataList)

        add_btn.setOnClickListener {
            val addTodoFragment = AddTodoFragment2()
            supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_up, 0, 0, 0)
                .add(android.R.id.content, addTodoFragment)
                .commit()
        }
    }
}
