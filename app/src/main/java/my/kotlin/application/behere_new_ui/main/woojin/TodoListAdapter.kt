package my.kotlin.application.behere_new_ui.main.woojin

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_todo_list.view.*
import my.kotlin.application.behere_new_ui.R

class TodoListAdapter(private val context: Context, private val todoDataList: List<String>) :
    RecyclerView.Adapter<TodoListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.adapter_todo_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.todoTitle.text = todoDataList[position]
        holder.locationView.visibility = View.VISIBLE
        holder.memoView.visibility = View.VISIBLE

        holder.checkBox.setOnClickListener {
            if (!it.isSelected) {
                holder.todoTitle.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                it.isSelected = true
            } else {
                holder.todoTitle.paintFlags = Paint.ANTI_ALIAS_FLAG
                it.isSelected = false
            }
        }
    }

    override fun getItemCount(): Int {
        return todoDataList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val todoTitle: TextView = itemView.todo_title_txt
        val locationView: View = itemView.todo_location_layout
        val memoView: View = itemView.todo_memo_layout
        val checkBox: CheckBox = itemView.check_box
    }
}