package my.kotlin.application.behere_new_ui.main.woojin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.fragment_add_todo2.*
import my.kotlin.application.behere_new_ui.R


class AddTodoFragment2: DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_todo2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //키보드 on
        todo_edit.requestFocus()

        val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

        save_btn.setOnClickListener {
            dismiss()
        }

        close_btn.setOnClickListener {
            dismiss()
        }

        filter_btn.setOnClickListener {
            val settingLocationFragment = SettingLocationFragment()
            fragmentManager
                ?.beginTransaction()
                ?.setCustomAnimations(R.anim.slide_up, 0, 0, 0)
                ?.replace(R.id.fragment_content, settingLocationFragment)
                ?.commit()
        }
    }

    override fun onPause() {
        super.onPause()

        //키보드 off
        todo_edit.clearFocus()

        val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }
}