package my.kotlin.application.behere_new_ui.main.woojin

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.fragment_add_todo2.*
import kotlinx.android.synthetic.main.fragment_add_todo2.close_btn
import kotlinx.android.synthetic.main.fragment_setting_location.*
import my.kotlin.application.behere_new_ui.R

class SettingLocationFragment : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_setting_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        close_btn.setOnClickListener {
            dismiss()
        }

        ok_btn.setOnClickListener {
            dismiss()
        }
    }
}