package my.kotlin.application.behere_new_ui.main.woojin

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import my.kotlin.application.behere_new_ui.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_main)


        initializeViews()
    }

    private fun initializeViews() {
        val groupDataList = listOf("기본", "운동", "일", "공부")

        val groupList: RecyclerView = group_list
        groupList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        groupList.adapter = GroupListAdapter(this, groupDataList)

        val todoDataList = listOf("다이소가기", "퇴근하기", "쉬기")

        val todoList: RecyclerView = todo_list
        todoList.layoutManager = LinearLayoutManager(this)
        todoList.adapter = TodoListAdapter(this, todoDataList)

        add_btn.setOnClickListener {
            val addTodoFragment = AddTodoFragment()
            supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_up, 0, 0, 0)
                .add(R.id.add_fragment, addTodoFragment)
                .commit()
//            startActivity(Intent(this, AddTodoActivity::class.java))
        }
    }
}


