package my.kotlin.application.behere_new_ui.main.woojin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_add_todo.*
import kotlinx.android.synthetic.main.fragment_add_todo.view.*
import my.kotlin.application.behere_new_ui.R

class AddTodoFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_todo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val groupDataList = listOf("기본", "운동", "일", "공부","+추가")

        val groupList: RecyclerView = view.group_list
        groupList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        groupList.adapter = context?.let { GroupListAdapter(it, groupDataList) }

        done_btn.setOnClickListener {
            fragmentManager?.beginTransaction()?.remove(this)?.commit()
        }
    }
}